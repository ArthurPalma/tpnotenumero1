﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ClassLibrary1;


namespace Test_csharp
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void classessaitest()
        {
            int compteur = 5;
            Assert.AreEqual(4, Basiccounter.decrementation(compteur));
            Assert.AreEqual(6, Basiccounter.incrementation(compteur));
        }
    }
}
